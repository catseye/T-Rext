<!--
SPDX-FileCopyrightText: Chris Pressey, the original author of this work, has placed dedicated it to the public domain.
For more information, please refer to <https://unlicense.org/>
SPDX-License-Identifier: Unlicense
-->

    -> Functionality "Clean up punctuation and spaces" is implemented by
    -> shell command "python3 bin/t-rext %(test-body-file)"
